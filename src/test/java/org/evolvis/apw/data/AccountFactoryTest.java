package org.evolvis.apw.data;

import static org.junit.Assert.assertEquals;

import org.evolvis.apw.services.AccountFactory;
import org.evolvis.apw.services.UnknownAccountException;
import org.junit.Test;

public class AccountFactoryTest
{
	@Test
	public void testCreate() throws UnknownAccountException
	{
		AccountFactory factory = new AccountFactory();
		Account account = factory.create("zaphod@beeblebrox.org", "heartofgold");
		
		LevelWatcher watcher = new LevelWatcher();
		account.getWatchers().add(watcher);
		watcher.setLevel(new Price("Amazon.de", 42.5, "EUR", Price.CONDITION_NEW));
		Product product = new Product("ABC123", "Yoyodyne Play Station");
		product.getPrices().add(new Price("Amazon.de", 199.95, "EUR", Price.CONDITION_NEW));
		watcher.setProduct(product);
		
		factory.update(account);

		assertEquals(account, factory.getByEmail("zaphod@beeblebrox.org", "heartofgold"));		
	}

	@Test
	public void testGetByEmail() throws UnknownAccountException
	{
		AccountFactory factory = new AccountFactory();
		Account account = factory.create("zaphod@beeblebrox.org", "heartofgold");
		assertEquals(account.getEmail(), factory.getByEmail("zaphod@beeblebrox.org", "heartofgold").getEmail());
	}

}
