package net.sourceforge.stripes.velocity;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.controller.ActionResolver;
import net.sourceforge.stripes.controller.StripesConstants;
import net.sourceforge.stripes.localization.LocalizationUtility;
import net.sourceforge.stripes.util.CryptoUtil;
import net.sourceforge.stripes.validation.ValidationError;

import org.apache.velocity.context.Context;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.tools.config.DefaultKey;
import org.apache.velocity.tools.generic.AbstractLockConfig;
import org.apache.velocity.tools.generic.ValueParser;

@DefaultKey("stripes")
public class StripesTool extends AbstractLockConfig {
	protected Log log;
	protected Context velocityContext;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected String actionPath;
	protected ActionBean actionBean;
	protected String eventName;
	
	protected Map<String, List<String>> errors;
	
	@Override
	protected void configure(ValueParser valueParser) {
		log = (Log) valueParser.get("log");
		velocityContext = (Context) valueParser.get("velocityContext");
		
		request = (HttpServletRequest) valueParser.get("request");
		response = (HttpServletResponse) valueParser.get("response");
		actionPath = (String) request.getAttribute(ActionResolver.RESOLVED_ACTION);
		actionBean = (ActionBean) request.getAttribute(StripesConstants.REQ_ATTR_ACTION_BEAN);
		eventName = (String) request.getAttribute(StripesConstants.REQ_ATTR_EVENT_NAME);
	}
	
	public String labelFor(String name) {
		if (false)
		System.out.println("find " + name + " for " +
				actionPath + " / " +
				actionBean.getClass().getSimpleName() + " / " +
				request.getLocale());
		return LocalizationUtility.getLocalizedFieldName(
				name,
				actionPath,
				actionBean.getClass(),
				request.getLocale());
	}
	
	/*
	public String urlFor(String x) {
		String base = getPreferredBaseUrl();
		UrlBuilder builder = new UrlBuilder(request.getLocale(), base, false);
		
		String url = builder.toString();
		String contextPath = request.getContextPath();
		if (contextPath.length() > 1 && !url.startsWith(contextPath + '/'))
				url = contextPath + url;
		
		return response.encodeURL(url);
	}
	
	protected String getPreferredBaseUrl() throws StripesJspException {
		// If the beanclass attribute was supplied we'll prefer that to an href
		if (this.beanclass != null) {
			String beanHref = getActionBeanUrl(beanclass);
			if (beanHref == null) {
				throw new StripesJspException("The value supplied for the 'beanclass' attribute " + "does not represent a valid ActionBean. The value supplied was '" + this.beanclass + "'. If you're prototyping, or your bean isn't ready yet "
				        + "and you want this exception to go away, just use 'href' for now instead.");
			} else {
				return beanHref;
			}
		} else {
			return getUrl();
		}
	}
	*/
	
	protected Iterable<Map.Entry<String, List<ValidationError>>> getErrorIterator() {
		return actionBean.getContext().getValidationErrors().entrySet();
	}
	
	public Map<String, List<String>> getErrors() {
		if (errors != null)
			return errors;
		
		errors = new LinkedHashMap<String, List<String>>();
		List<String> all = new LinkedList<String>();
		
		for (Map.Entry<String, List<ValidationError>> entry : getErrorIterator()) {
			final String key = entry.getKey();
			List<String> cur = new LinkedList<String>();
			for (ValidationError error : entry.getValue()) {
				String message = error.getMessage(request.getLocale());
				if (message.length() != 0) {
					all.add(message);
					cur.add(message);
				}
			}
			
			if (!cur.isEmpty()) {
				errors.put(key, Collections.unmodifiableList(cur));
			}
		}
		
		if (!all.isEmpty()) {
			errors.put("all", Collections.unmodifiableList(all));
		}
		
		errors = Collections.unmodifiableMap(errors);
		
		return errors;
	}
	
	public boolean containsErrorFor(String key) {
		return getErrors().containsKey(key);
	}
	
	public void form() {
		
	}
	
	public String getFormHiddenFields() {
		StringBuffer out = new StringBuffer();
		out.append("<div style=\"display: none;\">");
        out.append("<input type=\"hidden\" name=\"");
        out.append(StripesConstants.URL_KEY_SOURCE_PAGE);
        out.append("\" value=\"");
        out.append(CryptoUtil.encrypt(request.getRequestURI()));
        out.append("\" />");
        out.append("</div>");
        return out.toString();
	}
}
