package org.evolvis.apw.servlet;

import net.sourceforge.stripes.config.Configuration;
import net.sourceforge.stripes.controller.ActionBeanContextFactory;
import net.sourceforge.stripes.controller.ActionBeanPropertyBinder;
import net.sourceforge.stripes.controller.ActionResolver;
import net.sourceforge.stripes.controller.StripesFilter;
import net.sourceforge.stripes.localization.LocalePicker;

import org.apache.velocity.tools.config.DefaultKey;

@DefaultKey(value="stripes")
public class StripesTool {
	public Configuration getConfiguration() {
		return StripesFilter.getConfiguration();
	}
	
	public ActionBeanContextFactory getActionBeanContextFactory() {
		return StripesFilter.getConfiguration().getActionBeanContextFactory();
	}
	
	public ActionBeanPropertyBinder getActionBeanPropertyBinder() {
		return StripesFilter.getConfiguration().getActionBeanPropertyBinder();
	}
	
	public ActionResolver getActionResolver() {
		return StripesFilter.getConfiguration().getActionResolver();
	}
	
	public LocalePicker getLocalePicker() {
		return StripesFilter.getConfiguration().getLocalePicker();
	}
}
