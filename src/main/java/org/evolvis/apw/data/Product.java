package org.evolvis.apw.data;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="products")
public class Product 
{
	private Long id = null;
	private String asin = null;
	private String name = null;
	private Set<Price> prices = new HashSet<Price>();
	
	public Product()
	{
		super();
	}
	
	public Product(String asin, String name)
	{
		super();
		this.asin = asin;
		this.name = name;
	}

	@Column(name="asin")
	public String getAsin()
	{
		return asin;
	}

	public void setAsin(String asin)
	{
		this.asin = asin;
	}
		
	@Column(name="name")
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
	@JoinColumn(name="product_id")
	public Set<Price> getPrices()
	{
		return prices;
	}
	
	public void setPrices(Set<Price> prices)
	{
		this.prices = prices;
	}
	
	@Id @GeneratedValue
	@Column(name="id")
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	@Override
    public String toString()
	{
		return getName() + " (" + getAsin() + ")" + ", known Prices: " + getPrices();
	}
}
