package org.evolvis.apw.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="prices")
public class Price 
{
	public static final int CONDITION_NEW = 0;
	public static final int CONDITION_USED = 1;
	
	private Long id = null;
	private Double amount = null;
	private String currency = null;
	private Date date = null;
	private String merchant;
	private int condition = -1;
	
	public Price()
	{
		super();
		this.date = new Date();
	}
	
	public Price(String merchant, Double amount, String currency, int condition)
	{
		this();
		this.setMerchant(merchant);
		this.setAmount(amount);
		this.setCurrency(currency);
		this.setCondition(condition);
	}

	public Price(String merchant, Double amount, String currency, int condition, Date date)
	{
		this(merchant, amount, currency, condition);
		this.setDate(date);
	}

	@Column(name="condition")
	public int getCondition()
	{
		return condition;
	}

	public void setCondition(int condition)
	{
		this.condition = condition;
	}

	@Column(name="amount")
	public Double getAmount()
	{
		return amount;
	}
	
	public void setAmount(Double amount)
	{
		this.amount = amount;
	}
		
	@Column(name="merchant")
	public String getMerchant()
	{
		return merchant;
	}

	public void setMerchant(String merchant)
	{
		this.merchant = merchant;
	}

	@Column(name="currency")
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(String currency)
	{
		this.currency = currency;
	}
	
	@Column(name="date")
	public Date getDate()
	{
		return date;
	}
	
	public void setDate(Date date)
	{
		this.date = date;
	}	
	
	@Id @GeneratedValue
	@Column(name="id")
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	@Override
    public String toString()
	{
		return getAmount() + " " + getCurrency() + " (Condition " + getCondition() + ") at " + getMerchant() + " reported at " + getDate();
	}
}
