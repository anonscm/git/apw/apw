package org.evolvis.apw.data;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("LevelWatcher")
public class LevelWatcher extends Watcher
{
	private Price level = null;

	@OneToOne(cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
	@JoinColumn(name="levelwatcher_id")
	public Price getLevel()
	{
		return level;
	}

	public void setLevel(Price level)
	{
		this.level = level;
	}
	
	@Override
    public String toString()
	{
		return super.toString() + " level check at " + getLevel();
	}
}
