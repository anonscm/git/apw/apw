package org.evolvis.apw.data;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="accounts")
public class Account 
{
	private Long id = null;
	private String email = null;
	private String password = null;
	private Set<Watcher> watchers = new HashSet<Watcher>();
	
	@Id @GeneratedValue
	@Column(name="id")
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	@Column(name="email")
	public String getEmail()
	{
		return email;
	}
	
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	@Column(name="password")
	public String getPassword()
	{
		return password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}

	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
	@JoinColumn(name="account_id")
	public Set<Watcher> getWatchers()
	{
		return watchers;
	}
	
	public void setWatchers(Set<Watcher> watchers)
	{
		this.watchers = watchers;
	}
	
	@Override
    public String toString()
	{
		return this.getClass().getCanonicalName() + " email=" + getEmail() + " watchers=" + getWatchers();
	}
}
