package org.evolvis.apw.services;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.ws.BindingProvider;

import com.amazon.webservices.awsecommerceservice.AWSECommerceService;
import com.amazon.webservices.awsecommerceservice.AWSECommerceServicePortType;
import com.amazon.webservices.awsecommerceservice.Item;
import com.amazon.webservices.awsecommerceservice.ItemSearch;
import com.amazon.webservices.awsecommerceservice.ItemSearchRequest;
import com.amazon.webservices.awsecommerceservice.ItemSearchResponse;
import com.amazon.webservices.awsecommerceservice.Items;
import com.amazon.webservices.awsecommerceservice.SearchResultsMap.SearchIndex;

public final class AmazonSearchService {
	private AmazonSearchService() {
		super();
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String args[]) throws Exception {
		AWSECommerceService service = new AWSECommerceService();
		AWSECommerceServicePortType portType = service.getAWSECommerceServicePort();
		BindingProvider bindingProvider = (BindingProvider) portType;
		
		System.out.println("Invoking itemSearch...");
		System.out.println(service.getServiceName());
		System.out.println(service.getWSDLDocumentLocation());
		System.out.println("endpoint: " + bindingProvider.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY));
		
		ItemSearchRequest itemSearchRequest = new ItemSearchRequest();
		itemSearchRequest.setKeywords("mario kart");
		//itemSearchRequest.setSearchIndex("All");
		itemSearchRequest.setSearchIndex("Blended");
		//itemSearchRequest.setSearchIndex("Merchants");
		itemSearchRequest.setCount(new BigInteger("16"));
		//itemSearchRequest.setTagPage(new BigInteger("10"));
		//itemSearchRequest.setTagsPerPage(new BigInteger("10"));
		
		// for images
		//itemSearchRequest.getResponseGroup().add("Images");
		// for ordering
		//itemSearchRequest.getResponseGroup().add("SalesRank");
        //request.getResponseGroup().add("Offers");
        //request.getResponseGroup().add("OfferFull");
        //request.getResponseGroup().add("ItemAttributes");
		itemSearchRequest.getResponseGroup().add("Medium");
		
		ItemSearch itemSearch = new ItemSearch();
		itemSearch.setAWSAccessKeyId("0MV3B3MRGAVH8P027YR2");
		itemSearch.getRequest().add(itemSearchRequest);
		
		ItemSearchResponse itemSearchResponse = portType.itemSearch(itemSearch);
		
		int indexItemCount = 0;
		int itemCount = 0;
		
		for (Items items : itemSearchResponse.getItems()) {
			System.out.println(items.getQid());
			System.out.println(items.getEngineQuery());
			System.out.println(items.getTotalPages());
			System.out.println(items.getTotalResults());
			System.out.println(items.getSearchBinSets());
			
			if (items.getSearchResultsMap() != null && items.getSearchResultsMap().getSearchIndex() != null) {
				List<SearchIndex> indexList = items.getSearchResultsMap().getSearchIndex();
				Collections.sort(indexList, new Comparator<SearchIndex>() {
					public int compare(SearchIndex o1, SearchIndex o2) {
					    return o1.getRelevanceRank().compareTo(o2.getRelevanceRank());
					}
				});
				
				for (SearchIndex index : indexList) {
					System.out.println("  " + index.getIndexName());
					System.out.println("    Pages           " + index.getPages());
					System.out.println("    Results         " + index.getResults());
					System.out.println("    RelevanceRank   " + index.getRelevanceRank());
					System.out.println("    Show --->       " + index.getASIN());
					indexItemCount += index.getASIN().size();
				}
			}
			
			if (items.getItem() != null && !items.getItem().isEmpty()) {
				List<Item> itemList = items.getItem();
				if (false)
				Collections.sort(itemList, new Comparator<Item>() {
					public int compare(Item o1, Item o2) {
						if (o1.getSalesRank() == null)
							return Integer.MAX_VALUE;
						else if (o2.getSalesRank() == null)
							return Integer.MIN_VALUE;
						else
							return
								Integer.parseInt(o1.getSalesRank()) -
								Integer.parseInt(o2.getSalesRank());
					}
				});
				
				for (Item item : itemList) {
					System.out.println("  " +
							item.getASIN() + " " +
							item.getItemAttributes().getTitle() + " (" +
							item.getSalesRank() + ")");
					
					if (false) {
						System.out.println("    " + item.getDetailPageURL());
						
						if (item.getSmallImage() != null)
							System.out.println("    image S " + item.getSmallImage().getURL());
						if (item.getMediumImage() != null)
							System.out.println("    image M " + item.getMediumImage().getURL());
						if (item.getLargeImage() != null)
							System.out.println("    image L " + item.getLargeImage().getURL());
					}
					itemCount++;
				}
			}
		}
		
		System.out.println("index item count: " + indexItemCount);
		System.out.println("item count: " + itemCount);
	}
}
