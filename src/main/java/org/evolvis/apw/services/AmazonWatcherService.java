
package org.evolvis.apw.services;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.evolvis.apw.data.Price;
import org.evolvis.apw.data.Product;

import com.amazon.webservices.awsecommerceservice.AWSECommerceService;
import com.amazon.webservices.awsecommerceservice.AWSECommerceServicePortType;
import com.amazon.webservices.awsecommerceservice.CustomerContentLookup;
import com.amazon.webservices.awsecommerceservice.CustomerContentLookupRequest;
import com.amazon.webservices.awsecommerceservice.CustomerContentLookupResponse;
import com.amazon.webservices.awsecommerceservice.CustomerContentSearch;
import com.amazon.webservices.awsecommerceservice.CustomerContentSearchRequest;
import com.amazon.webservices.awsecommerceservice.CustomerContentSearchResponse;
import com.amazon.webservices.awsecommerceservice.Errors;
import com.amazon.webservices.awsecommerceservice.Item;
import com.amazon.webservices.awsecommerceservice.ItemLookup;
import com.amazon.webservices.awsecommerceservice.ItemLookupRequest;
import com.amazon.webservices.awsecommerceservice.ItemLookupResponse;
import com.amazon.webservices.awsecommerceservice.ItemSearch;
import com.amazon.webservices.awsecommerceservice.ItemSearchRequest;
import com.amazon.webservices.awsecommerceservice.ItemSearchResponse;
import com.amazon.webservices.awsecommerceservice.ListItem;
import com.amazon.webservices.awsecommerceservice.ListLookup;
import com.amazon.webservices.awsecommerceservice.ListLookupRequest;
import com.amazon.webservices.awsecommerceservice.ListLookupResponse;
import com.amazon.webservices.awsecommerceservice.Offer;
import com.amazon.webservices.awsecommerceservice.Request;

public final class AmazonWatcherService 
{
	public static final int LOCALE_DEBUG = 0;
    public static final int LOCALE_US = 1;
    public static final int LOCALE_DE = 2;
    public static final int LOCALE_UK = 3;
    public static final int LOCALE_FR = 4;
    
    public static final String KEY_CONDITION_NEW = "New";
    public static final String KEY_CONDITION_USED = "Used";
    
    public static final String MERCHANTID_AMAZON_DE = "A3JWKAKR8XB7XF";
    // FIXME: add other amazon merchant ids for locales us, uk, fr
	
	private static final QName SERVICE_NAME = new QName("http://webservices.amazon.com/AWSECommerceService/2008-03-03", "AWSECommerceService");

    private static final String[] LOCALE_URLS = 
    {
    	"http://localhost:8000/onca/soap?Service=AWSECommerceService",
    	"http://soap.amazon.com/onca/soap?Service=AWSECommerceService",  	
    	"http://soap.amazon.de/onca/soap?Service=AWSECommerceService",    	
    	"http://soap.amazon.co.uk/onca/soap?Service=AWSECommerceService",    	
    	"http://soap.amazon.fr/onca/soap?Service=AWSECommerceService"    	
    };
    
    private int currentLocale = -1;
    
    // FIXME: externalize!
    private static String AccessKey = "0MV3B3MRGAVH8P027YR2";

    private static AWSECommerceService service = null;
    private static AWSECommerceServicePortType port = null;
    
    private class QueryResult
    {
    	boolean valid = false;
    	List<String> errorCode = null;
    	String errorMessage = null;

    	public QueryResult(Request request)
    	{
    		this.valid = Boolean.parseBoolean(request.getIsValid());
    		if (!isValid())
    		{
    			this.errorCode = new ArrayList<String>();
    			this.errorMessage = formatErrors(request.getErrors());
    		}
    	}
    	
    	private String formatErrors(Errors errors)
    	{
    		StringBuffer errorList = new StringBuffer();
    		Iterator<Errors.Error> iter = errors.getError().iterator();
    		while (iter.hasNext())
    		{
    			Errors.Error thisError = iter.next();
    			errorList.append(thisError.getMessage());
    			if (iter.hasNext()) 
    				errorList.append(", ");

    			getErrorCode().add(thisError.getCode());
    		}
    		
    		return errorList.toString();
    	}
    	
    	public List<String> getErrorCode()
		{
			return errorCode;
		}
		
    	public String getErrorMessage()
		{
			return errorMessage;
		}
		
    	public boolean isValid()
		{
			return valid;
		}
    }

    private AmazonWatcherService(String wsdlPath) 
    {
    	super();

    	URL wsdlURL = AWSECommerceService.WSDL_LOCATION;
        File wsdlFile = new File(wsdlPath);
        try 
        {
            if (wsdlFile.exists()) 
            {
                wsdlURL = wsdlFile.toURI().toURL();
            } 
            else 
            {
                wsdlURL = new URL(wsdlPath);
            }
        } 
        catch (MalformedURLException e) 
        {   	
            throw new RuntimeException(e);
        }
      
        service = new AWSECommerceService(wsdlURL, SERVICE_NAME);
        port = service.getAWSECommerceServicePort(); 
        setLocale(LOCALE_US);
    }

    /**
     * Sets the locale to be used in requests. Valid locales include
     * AmazonWatcherService.LOCALE_DEBUG, AmazonWatcherService.LOCALE_US and
     * AmazonWatcherService.LOCALE_DE and others.
     * 
     * @param locale locale to be used.
     */
    public void setLocale(int locale)
    {
    	this.currentLocale = locale;
        BindingProvider binding = (BindingProvider)port;
        binding.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, LOCALE_URLS[this.currentLocale]);    	
    }

    /**
     * Returns the current locale.
     * 
     * @return the locale.
     */
    public int getLocale()
    {
    	return currentLocale;
    }
    
    /**
     * Besides the unique email address that every customer used to log in to Amazon,
     * the primary key for any customer operations is the Amazon-generated customerId.
     * This method resolves the customerId from the email address. <b>CAUTION:</b> this
     * <b>only</b> works in the US locale, maybe for data protection considerations. 
     * 
     * @param customerEMail
     * @return Amazon customerId
     * @throws AmazonServiceException
     * @throws TooManyResultsException
     * @throws TooFewResultsException
     */
    public String lookupCustomerId(String customerEMail) throws AmazonServiceException, TooManyResultsException, TooFewResultsException
    {
    	if (getLocale()!=LOCALE_DEBUG && getLocale()!=LOCALE_US)
    		throw new AmazonServiceException("Operation lookupCustomerId works only in US locale! Current locale is " + getLocale());
    	
    	CustomerContentSearch body = new CustomerContentSearch();
        body.setAWSAccessKeyId(AccessKey);
        
        CustomerContentSearchRequest request = new CustomerContentSearchRequest();
        request.setEmail(customerEMail);
        body.setShared(request);
        
        CustomerContentSearchResponse response = port.customerContentSearch(body);
        QueryResult result = new QueryResult(response.getCustomers().get(0).getRequest());
        
        if (!result.isValid())
        	throw new AmazonServiceException(result.getErrorMessage());
        else if (response.getCustomers().get(0).getTotalResults().intValue()==0)
        	throw new TooFewResultsException(response.getCustomers().get(0).getTotalResults());
        else if (response.getCustomers().get(0).getTotalResults().intValue()!=1)
        	throw new TooManyResultsException(response.getCustomers().get(0).getTotalResults());
        else 
        	return response.getCustomers().get(0).getCustomer().get(0).getCustomerId();
    }

    /**
     * Retrieves the wishlistId from the customerId. The first found wishlist is
     * used.
     * 
     * @param amazonId
     * @return wishlistId
     * @throws AmazonServiceException
     * @throws TooFewResultsException
     * @throws TooManyResultsException
     */
    public String lookupWishlistId(String amazonId) throws AmazonServiceException, TooFewResultsException, TooManyResultsException
    {
    	// FIXME: add wishlistId lists for multiple wishlist support. 
    	CustomerContentLookup body = new CustomerContentLookup();
        body.setAWSAccessKeyId(AccessKey);
        
        CustomerContentLookupRequest request = new CustomerContentLookupRequest();
        request.setCustomerId(amazonId);
        request.getResponseGroup().add("CustomerLists");
        body.setShared(request);
        
        CustomerContentLookupResponse response = port.customerContentLookup(body);
        QueryResult result = new QueryResult(response.getCustomers().get(0).getRequest());

        if (!result.isValid())
        	throw new AmazonServiceException(result.getErrorMessage());
        else if (response.getCustomers().get(0).getTotalResults().intValue()==0)
        	throw new TooFewResultsException(response.getCustomers().get(0).getTotalResults());
        else if (response.getCustomers().get(0).getTotalResults().intValue()!=1)
        	throw new TooManyResultsException(response.getCustomers().get(0).getTotalResults());
        else 
        	return response.getCustomers().get(0).getCustomer().get(0).getWishListId();
    }

    /**
     * Retrieves a product from an ASIN. The returned Product instance includes 
     * all current price information.
     * 
     * @param asin
     * @return Product instance.
     * @throws AmazonServiceException
     * @throws TooFewResultsException
     * @throws TooManyResultsException
     */
    public Product lookupProduct(String asin) throws AmazonServiceException, TooFewResultsException, TooManyResultsException
    {
    	Product product = new Product();
    	
    	ItemLookup body = new ItemLookup();
        body.setAWSAccessKeyId(AccessKey);

        ItemLookupRequest request = new ItemLookupRequest();
        request.setIdType("ASIN");
        request.getItemId().add(asin);
        request.getResponseGroup().add("OfferFull");
        request.getResponseGroup().add("ItemAttributes");
        body.setShared(request);

        ItemLookupResponse response = port.itemLookup(body);
        QueryResult result = new QueryResult(response.getItems().get(0).getRequest());

        if (!result.isValid())
        	throw new AmazonServiceException(result.getErrorMessage());
        else if (response.getItems().get(0).getTotalResults().intValue()==0)
        	throw new TooFewResultsException(response.getItems().get(0).getTotalResults());
        else if (response.getItems().get(0).getTotalResults().intValue()!=1)
        	throw new TooManyResultsException(response.getItems().get(0).getTotalResults());
        else 
        {
            Item item = response.getItems().get(0).getItem().get(0);
            product = parseProduct(item);        	
        }

        return product;
    }

    /**
     * Returns only the current prices for the given Product instance.
     * 
     * @param product
     * @return Set of current price information.
     * @throws AmazonServiceException
     * @throws TooFewResultsException
     * @throws TooManyResultsException
     */
    public Set<Price> lookupPrices(Product product) throws AmazonServiceException, TooFewResultsException, TooManyResultsException
    {
    	Product refreshedProduct = lookupProduct(product.getAsin());
    	return refreshedProduct.getPrices();
    }
    
    /**
     * Returns all products on the given wishlist (identified by the given wishlistId)
     * complete with all current price information.
     * 
     * @param wishlistId
     * @return List of Product instances.
     * @throws AmazonServiceException
     * @throws TooFewResultsException
     * @throws TooManyResultsException
     */
    public List<Product> getWishlistProducts(String wishlistId) throws AmazonServiceException, TooFewResultsException, TooManyResultsException
    {
    	int pages = getWishlistPageCount(wishlistId);
    	
    	List<Product> products = new ArrayList<Product>();
    	for (int i=1; i<=pages; i++)
    	{
    		products.addAll(getWishlistPage(wishlistId, i));    	
    		if (getLocale()==LOCALE_DEBUG)
    			try
	    		{
    				// This is needed for the soap monitor lag. Apache sucks!
	    			Thread.sleep(5000);
	    		}
    			catch (InterruptedException e)
    			{
    				throw new RuntimeException(e);
    			}
    	}    	
    	return products;
    }
    
    /**
     * Searches for products on Amazon. Keywords have to be space seperated. The
     * searchIndex indicates which index should be used for searching, for example
     * "Toys". Searching in all indexes is done by giving searchIndex "Blended". 
     * Using blended search returns Product instances without price information.
     * Searching in all other indexes returns complete current price information.
     * 
     * @param keywords
     * @param searchIndex
     * @return List of Product instances.
     * @throws AmazonServiceException
     * @throws TooFewResultsException
     * @throws TooManyResultsException
     */
    public List<Product> searchProducts(String keywords, String searchIndex) throws AmazonServiceException, TooFewResultsException, TooManyResultsException
    {
    	List<Product> products = new ArrayList<Product>();
    	
    	ItemSearch body = new ItemSearch();
        body.setAWSAccessKeyId(AccessKey);

        ItemSearchRequest request = new ItemSearchRequest();
        request.setSearchIndex(searchIndex);
        try
		{
			request.setKeywords(URLEncoder.encode(keywords, "UTF-8"));
		}
		catch (UnsupportedEncodingException e)
		{
			throw new RuntimeException(e);
		}
        request.getResponseGroup().add("Offers");
        request.getResponseGroup().add("OfferFull");
        request.getResponseGroup().add("ItemAttributes");
        body.setShared(request);

        ItemSearchResponse response = port.itemSearch(body);
        QueryResult result = new QueryResult(response.getItems().get(0).getRequest());

        if (!result.isValid())
        	throw new AmazonServiceException(result.getErrorMessage());
        else if (response.getItems().get(0).getTotalResults().intValue()==0)
        	throw new TooFewResultsException(response.getItems().get(0).getTotalResults());
        else 
        {
        	Iterator<Item> iter = response.getItems().get(0).getItem().iterator();
        	while (iter.hasNext())
        	{
            	Item thisItem = iter.next();
                products.add(parseProduct(thisItem));        		
        	}
        }

        return products;
    }

	private Product parseProduct(Item item)
	{
    	Product product = new Product();

    	product.setAsin(item.getASIN());
		product.setName(item.getItemAttributes().getTitle());
		
		List<Offer> offers = item.getOffers().getOffer();
		Iterator<Offer> iter = offers.iterator();
		while (iter.hasNext())
		{
			Offer thisOffer = iter.next();

			Price thisPrice = new Price();
			thisPrice.setMerchant(thisOffer.getMerchant().getMerchantId());
			thisPrice.setAmount(thisOffer.getOfferListing().get(0).getPrice().getAmount().doubleValue()/100);
			thisPrice.setCurrency(thisOffer.getOfferListing().get(0).getPrice().getCurrencyCode());
			if (thisOffer.getOfferAttributes().getCondition()!=null)
				if (thisOffer.getOfferAttributes().getCondition().equals(KEY_CONDITION_NEW))
					thisPrice.setCondition(Price.CONDITION_NEW);
				else if (thisOffer.getOfferAttributes().getCondition().equals(KEY_CONDITION_USED))
					thisPrice.setCondition(Price.CONDITION_USED);
			
			product.getPrices().add(thisPrice);
		}
		
		return product;
	}

    private List<Product> getWishlistPage(String wishlistId, int page) throws AmazonServiceException, TooFewResultsException, TooManyResultsException
    {
    	List<Product> products = new ArrayList<Product>();
    	
    	ListLookup body = new ListLookup();
    	body.setAWSAccessKeyId(AccessKey);
    	
    	ListLookupRequest request = new ListLookupRequest();
    	request.setListId(wishlistId);
    	request.setListType("WishList");
    	request.setProductPage(BigInteger.valueOf(page));
    	request.getResponseGroup().add("ListFull");
        request.getResponseGroup().add("Offers");
        request.getResponseGroup().add("ItemAttributes");
        body.setShared(request);
    	
    	ListLookupResponse response = port.listLookup(body);
        QueryResult result = new QueryResult(response.getLists().get(0).getRequest());

        if (!result.isValid())
        	throw new AmazonServiceException(result.getErrorMessage());
        else if (response.getLists().get(0).getList().get(0).getTotalItems().intValue()==0)
        	throw new TooFewResultsException(response.getLists().get(0).getList().get(0).getTotalItems());
        else 
        {
            List<ListItem> list = response.getLists().get(0).getList().get(0).getListItem();
            Iterator<ListItem> iter = list.iterator();
            while (iter.hasNext())
            {
            	ListItem thisListItem = iter.next();
            	Item thisItem = thisListItem.getItem();
            	products.add(parseProduct(thisItem));
            }
        }

        return products;    	
    }
    
    private int getWishlistPageCount(String wishlistId) throws AmazonServiceException, TooFewResultsException, TooManyResultsException
    {
    	ListLookup body = new ListLookup();
    	body.setAWSAccessKeyId(AccessKey);
    	
    	ListLookupRequest request = new ListLookupRequest();
    	request.setListId(wishlistId);
    	request.setListType("WishList");
        body.setShared(request);
    	
    	ListLookupResponse response = port.listLookup(body);
        QueryResult result = new QueryResult(response.getLists().get(0).getRequest());

        if (!result.isValid())
        	throw new AmazonServiceException(result.getErrorMessage());
        else if (response.getLists().get(0).getList().get(0).getTotalItems().intValue()==0)
        	throw new TooFewResultsException(response.getLists().get(0).getList().get(0).getTotalItems());
        else 
        	return response.getLists().get(0).getList().get(0).getTotalPages().intValue();    	
    }

    @SuppressWarnings("unchecked")
	public static void main(String args[]) throws Exception 
    {
        AmazonWatcherService service = new AmazonWatcherService(args[0]);
        service.setLocale(AmazonWatcherService.LOCALE_DEBUG);
        System.out.println(service.searchProducts("Munchkin", "Toys"));        
    }
}
