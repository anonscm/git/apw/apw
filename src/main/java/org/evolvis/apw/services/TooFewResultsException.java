package org.evolvis.apw.services;

import java.math.BigInteger;

public class TooFewResultsException extends ResultNumberException
{
	private static final long serialVersionUID = 4837053304895826730L;

	public TooFewResultsException(BigInteger count)
	{
		super(count);
	}
}
