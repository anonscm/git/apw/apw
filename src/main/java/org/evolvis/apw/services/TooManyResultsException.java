package org.evolvis.apw.services;

import java.math.BigInteger;

public class TooManyResultsException extends ResultNumberException
{
	private static final long serialVersionUID = 3401087148526546061L;

	public TooManyResultsException(BigInteger count)
	{
		super(count);
	}
}
