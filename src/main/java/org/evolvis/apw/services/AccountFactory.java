package org.evolvis.apw.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.commons.codec.digest.DigestUtils;
import org.evolvis.apw.data.Account;

public class AccountFactory
{
	EntityManagerFactory entityManagerFactory = null;
	EntityManager entityManager = null;

	public AccountFactory()
	{
		entityManagerFactory = Persistence.createEntityManagerFactory("db");
		entityManager = entityManagerFactory.createEntityManager();		
	}
	
	@Override
	protected void finalize() throws Throwable
	{
		super.finalize();
		entityManager.close();
		entityManagerFactory.close();
	}
	
	private String encryptPassword(String password)
	{
		return DigestUtils.md5Hex(password);
	}
	
	@SuppressWarnings("unchecked")
	public Account getByEmail(String email, String password) throws UnknownAccountException
	{
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		
		Query query = entityManager.createQuery("SELECT a FROM Account a WHERE a.email=? AND a.password=?");
		query.setParameter(1, email);
		query.setParameter(2, encryptPassword(password));
		List<Account> accounts = query.getResultList();
		
		tx.commit();

		if (accounts!=null && accounts.size()>0)
			return accounts.get(0);
		else
			throw new UnknownAccountException();
	}

	public Account create(String email, String password)
	{
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		
		Account account = new Account();
		account.setEmail(email);
		account.setPassword(DigestUtils.md5Hex(password));

		entityManager.persist(account);
		
		tx.commit();

		return account;
	}
	
	public void update(Account account)
	{
		entityManager.merge(account);
	}
}
