package org.evolvis.apw.services;

import java.math.BigInteger;

public class ResultNumberException extends Exception
{
	private static final long serialVersionUID = 8929592896092521139L;
	
	private BigInteger count = null;

	public ResultNumberException(BigInteger count)
	{
		super("Wrong number of results");
		this.count = count;
	}

	@Override
	public String getMessage()
	{
		return super.getMessage() + " (" + count.toString() + ")";
	}
}
