package org.evolvis.apw.services;

public class AmazonServiceException extends Exception
{
	private static final long serialVersionUID = 858956913533318322L;

	public AmazonServiceException(String errorMessage)
	{
		super(errorMessage);
	}
}
