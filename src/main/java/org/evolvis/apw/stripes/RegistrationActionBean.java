package org.evolvis.apw.stripes;

import java.io.IOException;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;

@UrlBinding("/register")
public class RegistrationActionBean implements ActionBean {
	private ActionBeanContext context;
	
	public ActionBeanContext getContext() {
		return context;
	}
	
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
	
	@DefaultHandler
	public Resolution showRegistrationForm() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		
		System.out.println("show registration form");
		
		if (getContext().getRequest().getParameter("jsp") != null)
			return new ForwardResolution("/jsp/test.jsp");
		
		return new ForwardResolution("/velocity/showregister.vm");
	}
	
	@Validate(on = "registerUser", required = true, trim = true, minlength = 6, maxlength = 100)
	private String emailAddress;
	@Validate(on = "registerUser", required = true, trim = true, minlength = 6, maxlength = 100)
	private String password;
	@Validate(on = "registerUser", required = true, trim = true, minlength = 6, maxlength = 100)
	private String password2;
	
	public String getEmailAddress() {
    	return emailAddress;
    }

	public void setEmailAddress(String emailAddress) {
    	this.emailAddress = emailAddress;
    }

	public String getPassword() {
    	return password;
    }

	public void setPassword(String password) {
    	this.password = password;
    }

	public String getPassword2() {
    	return password2;
    }

	public void setPassword2(String password2) {
    	this.password2 = password2;
    }

	public Resolution registerUser() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		
		System.out.println("register user");
		System.out.println("  email " + getEmailAddress());
		System.out.println("  pw1   " + getPassword());
		System.out.println("  pw2   " + getPassword2());
		
		return showRegistrationForm();
	}
}
