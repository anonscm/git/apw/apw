package org.evolvis.apw.stripes;

import java.io.IOException;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.controller.DispatcherHelper;
import net.sourceforge.stripes.controller.StripesFilter;

@UrlBinding("/")
public class SearchFormActionBean implements ActionBean {
	private ActionBeanContext context;
	
	public ActionBeanContext getContext() {
		return context;
	}
	
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
	
	@DefaultHandler
	public Resolution index() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		
		System.out.println(getContext().getMessages());
		
		System.out.println(DispatcherHelper.getPageContext());
		
		System.out.println(StripesFilter.getConfiguration());
		
		if (getContext().getRequest().getParameter("groovy") != null) {
			return new ForwardResolution("/groovy/test.groovy");
		}
		
		return new ForwardResolution("/velocity/index.vm");
	}
}
