package org.evolvis.apw.stripes;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import com.amazon.webservices.awsecommerceservice.AWSECommerceService;
import com.amazon.webservices.awsecommerceservice.AWSECommerceServicePortType;
import com.amazon.webservices.awsecommerceservice.ItemLookup;
import com.amazon.webservices.awsecommerceservice.ItemLookupRequest;
import com.amazon.webservices.awsecommerceservice.ItemLookupResponse;

@UrlBinding("/detail/{asin}/{title}")
public class ShowDetailActionBean implements ActionBean {
	private static final AWSECommerceService service = new AWSECommerceService();
	private static final AWSECommerceServicePortType portType = service.getAWSECommerceServicePort();
	
	static {
		((BindingProvider) portType).getRequestContext().put(
				BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				"http://soap.amazon.de/onca/soap?Service=AWSECommerceService");
	}
	
	private static final Map<String, ItemLookupResponse> amazonDetailCache = new LinkedHashMap<String, ItemLookupResponse>();

	private ActionBeanContext context;
	
	public ActionBeanContext getContext() {
		return context;
	}
	
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
	
	private String asin;
	private String title;
	private ItemLookupResponse response;
	
	@DefaultHandler
	public Resolution search() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		
		System.out.println("asin:  " + getAsin());
		System.out.println("title: " + getTitle());
		
		if (getAsin() == null || getAsin().length() == 0)
			return new RedirectResolution("/");
		
		if (amazonDetailCache.containsKey(getAsin())) {
			System.out.println("Return response from amazon cache...");
			setResponse(amazonDetailCache.get(getAsin()));
			return new ForwardResolution("/velocity/detail.vm");
		}
		
		long amazonRequestStartTime = System.currentTimeMillis();
		
		ItemLookupRequest itemLookupRequest = new ItemLookupRequest();
		itemLookupRequest.setIdType("ASIN");
		itemLookupRequest.getItemId().add(getAsin());
		itemLookupRequest.getResponseGroup().add("Large");
		
		ItemLookup itemLookup = new ItemLookup();
		itemLookup.setAWSAccessKeyId("0MV3B3MRGAVH8P027YR2");
		itemLookup.getRequest().add(itemLookupRequest);
		
        System.out.println("Build lookup in: " + (System.currentTimeMillis() - amazonRequestStartTime) + "ms");
        amazonRequestStartTime = System.currentTimeMillis();
		
		setResponse(portType.itemLookup(itemLookup));
		
        System.out.println("Submit lookup to amazon in: " + (System.currentTimeMillis() - amazonRequestStartTime) + "ms");
        amazonRequestStartTime = System.currentTimeMillis();
        
        amazonDetailCache.put(getAsin(), getResponse());
		return new ForwardResolution("/velocity/detail.vm");
	}

	public String getAsin() {
    	return asin;
    }
	
	public void setAsin(String asin) {
    	this.asin = asin;
    }

	public String getTitle() {
    	return title;
    }

	public void setTitle(String title) {
    	this.title = title;
    }

	public ItemLookupResponse getResponse() {
    	return response;
    }

	public void setResponse(ItemLookupResponse response) {
    	this.response = response;
    }
}
