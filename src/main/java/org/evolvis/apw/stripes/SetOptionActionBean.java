package org.evolvis.apw.stripes;

import java.io.IOException;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.StreamingResolution;
import net.sourceforge.stripes.action.UrlBinding;

@UrlBinding("/setoption/{key}/{value}")
public class SetOptionActionBean implements ActionBean {
	private ActionBeanContext context;
	
	public ActionBeanContext getContext() {
		return context;
	}
	
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
	
	private String key;
	private String value;

	@DefaultHandler
	public Resolution search() throws IOException {
		// TODO verify key and values!!!
		
		if (getContext().getRequest().getSession().getAttribute(key) != null) {
			System.out.println("Override session " + key + " = " + value);
		} else {
			System.out.println("Set session " + key + " = " + value);
		}
		
		getContext().getRequest().getSession().setAttribute(key, value);
		
		return new StreamingResolution("text/plain", "");
	}
	
	public String getKey() {
    	return key;
    }

	public void setKey(String key) {
    	this.key = key;
    }

	public String getValue() {
    	return value;
    }

	public void setValue(String value) {
    	this.value = value;
    }
	
}
