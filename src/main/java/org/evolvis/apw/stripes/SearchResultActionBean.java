package org.evolvis.apw.stripes;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import com.amazon.webservices.awsecommerceservice.AWSECommerceService;
import com.amazon.webservices.awsecommerceservice.AWSECommerceServicePortType;
import com.amazon.webservices.awsecommerceservice.Item;
import com.amazon.webservices.awsecommerceservice.ItemSearch;
import com.amazon.webservices.awsecommerceservice.ItemSearchRequest;
import com.amazon.webservices.awsecommerceservice.ItemSearchResponse;
import com.amazon.webservices.awsecommerceservice.Items;
import com.amazon.webservices.awsecommerceservice.SearchResultsMap.SearchIndex;

@UrlBinding("/search/{q}")
public class SearchResultActionBean implements ActionBean {
	private static final AWSECommerceService service = new AWSECommerceService();
	private static final AWSECommerceServicePortType portType = service.getAWSECommerceServicePort();
	
	static {
		((BindingProvider) portType).getRequestContext().put(
				BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				"http://soap.amazon.de/onca/soap?Service=AWSECommerceService");
	}

	private static final Map<String, ItemSearchResponse> amazonSearchCache = new LinkedHashMap<String, ItemSearchResponse>();

	private ActionBeanContext context;
	
	public ActionBeanContext getContext() {
		return context;
	}
	
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
	
	private String q;
	private ItemSearchResponse response;
	
	@DefaultHandler
	public Resolution search() throws IOException {
		getContext().getResponse().setContentType("text/html; charset=UTF-8");
		
		if (getQ() == null || getQ().length() == 0)
			return new ForwardResolution("/velocity/result2.vm");
		
		if (amazonSearchCache.containsKey(getQ())) {
			System.out.println("Return response from amazon cache...");
			setResponse(amazonSearchCache.get(getQ()));
			return new ForwardResolution("/velocity/result2.vm");
		}
		
		long amazonRequestStartTime = System.currentTimeMillis();
		
		ItemSearchRequest itemSearchRequest = new ItemSearchRequest();
		itemSearchRequest.setSearchIndex("Blended");
		//itemSearchRequest.setSearchIndex("VideoGames");
		itemSearchRequest.setKeywords(getQ());
		itemSearchRequest.getResponseGroup().add("Medium");
		itemSearchRequest.setCount(new BigInteger("10"));
		
		ItemSearch itemSearch = new ItemSearch();
		itemSearch.setAWSAccessKeyId("0MV3B3MRGAVH8P027YR2");
		itemSearch.getRequest().add(itemSearchRequest);
		
        System.out.println("Build search in: " + (System.currentTimeMillis() - amazonRequestStartTime) + "ms");
        amazonRequestStartTime = System.currentTimeMillis();
		
		setResponse(portType.itemSearch(itemSearch));
		
        System.out.println("Submit request to amazon in: " + (System.currentTimeMillis() - amazonRequestStartTime) + "ms");
        amazonRequestStartTime = System.currentTimeMillis();
        
		for (Items items : getResponse().getItems()) {
			if (items.getSearchResultsMap() != null && items.getSearchResultsMap().getSearchIndex() != null && !items.getSearchResultsMap().getSearchIndex().isEmpty()) {
				Collections.sort(
						items.getSearchResultsMap().getSearchIndex(),
						new Comparator<SearchIndex>() {
							public int compare(SearchIndex o1, SearchIndex o2) {
							    return o1.getRelevanceRank().compareTo(o2.getRelevanceRank());
							}
						});
			}
			
			if (items.getItem() != null && !items.getItem().isEmpty()) {
				Collections.sort(
						items.getItem(),
						new Comparator<Item>() {
							public int compare(Item o1, Item o2) {
								if (o1.getSalesRank() == null)
									return Integer.MAX_VALUE;
								else if (o2.getSalesRank() == null)
									return Integer.MIN_VALUE;
								else
									return
										Integer.parseInt(o1.getSalesRank()) -
										Integer.parseInt(o2.getSalesRank());
							}
						});
			}
		}
		
        System.out.println("Sort response in: " + (System.currentTimeMillis() - amazonRequestStartTime) + "ms");
        System.out.println();
        
        amazonSearchCache.put(getQ(), getResponse());
		return new ForwardResolution("/velocity/result2.vm");
	}

	public String getQ() {
    	return q;
    }

	public void setQ(String q) {
    	this.q = q;
    }

	public ItemSearchResponse getResponse() {
    	return response;
    }

	public void setResponse(ItemSearchResponse response) {
    	this.response = response;
    }
}
