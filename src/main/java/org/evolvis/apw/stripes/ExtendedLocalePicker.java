package org.evolvis.apw.stripes;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sourceforge.stripes.localization.DefaultLocalePicker;

public class ExtendedLocalePicker extends DefaultLocalePicker {
	@Override
	public Locale pickLocale(HttpServletRequest request) {
	    HttpSession session = request.getSession();
	    Locale locale = (Locale) session.getAttribute("locale");
	    
	    if (request.getParameter("locale") != null) {
    		locale = new Locale(request.getParameter("locale"));
	    }
	    if (locale != null) {
	    	return locale;
	    }
	    
	    locale = super.pickLocale(request);
	    session.setAttribute("locale", locale);
	    return locale;
	}
}
