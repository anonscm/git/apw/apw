package org.evolvis.apw.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class UserMain implements EntryPoint {
	private PopupPanel currentPopupPanel = null;
	
	public void onModuleLoad() {
		if (RootPanel.get("shopSelectSpan") != null) {
			// remove all childrens
			Element element = RootPanel.get("shopSelectSpan").getElement();
			while (element.getChildNodes().getLength() > 0) {
				element.removeChild(element.getChildNodes().getItem(0));
			}
			
			// add new button
			final Hyperlink shopSelect = new Hyperlink() {
				@Override
				public void setText(String text) {
				    super.setHTML(text + " &#9660;");
				}
			};
			shopSelect.addStyleName("search");
			shopSelect.addClickListener(new ClickListener() {
				public void onClick(Widget sender) {
					if (currentPopupPanel != null && currentPopupPanel instanceof ChooseShopDialog) {
						currentPopupPanel.hide();
						currentPopupPanel = null;
					} else {
						int left = shopSelect.getAbsoluteLeft();
						int top = shopSelect.getAbsoluteTop() + shopSelect.getOffsetHeight();
						currentPopupPanel = new ChooseShopDialog(shopSelect);
						currentPopupPanel.setPopupPosition(left, top);
						currentPopupPanel.show();
					}
				}
			});
			
			shopSelect.setText("Amazon.de");
			
			RootPanel.get("shopSelectSpan").add(shopSelect);
		}
		
		if (RootPanel.get("categorySelectSpan") != null) {
			// remove all childrens
			Element element = RootPanel.get("categorySelectSpan").getElement();
			while (element.getChildNodes().getLength() > 0) {
				element.removeChild(element.getChildNodes().getItem(0));
			}
			
			// add new button
			final Hyperlink categorySelect = new Hyperlink() {
				@Override
				public void setText(String text) {
				    super.setHTML(text + " &#9660;");
				}
			};
			categorySelect.addStyleName("search");
			categorySelect.addClickListener(new ClickListener() {
				public void onClick(Widget sender) {
					if (currentPopupPanel != null && currentPopupPanel instanceof ChooseCategoryDialog) {
						currentPopupPanel.hide();
						currentPopupPanel = null;
					} else {
						int left = categorySelect.getAbsoluteLeft();
						int top = categorySelect.getAbsoluteTop() + categorySelect.getOffsetHeight();
						currentPopupPanel = new ChooseCategoryDialog(categorySelect);
						currentPopupPanel.setPopupPosition(left, top);
						currentPopupPanel.show();
					}
				}
			});
			
			categorySelect.setText("Allen Kategorien");
			
			RootPanel.get("categorySelectSpan").add(categorySelect);
		}
		
		RootPanel.get().add(new Spinner(0, 100, 1, 80));
		
	}
}
