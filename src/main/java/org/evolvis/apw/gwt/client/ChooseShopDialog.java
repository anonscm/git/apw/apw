package org.evolvis.apw.gwt.client;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ChooseShopDialog extends PopupPanel {
	private static final String[] VALUES = new String[] {
			"<strong>All</strong> notice that this will be slower!",
			"<strong>Amazon.com</strong>",
			"<strong>Amazon.de</strong>",
			"<strong>Amazon.co.uk</strong>" };

	public ChooseShopDialog(final HasText senderWidget) {
		super(true);
		
		FlexTable table = new FlexTable();
		table.setCellPadding(0);
		table.setCellSpacing(0);
		
		int row = 0;
		
		for (String title : VALUES) {
			final Hyperlink hyperlink = new Hyperlink();
			hyperlink.setHTML(title);
			hyperlink.addClickListener(new ClickListener() {
				public void onClick(Widget sender) {
					if (senderWidget != null)
						senderWidget.setText(hyperlink.getText());
					hide();
					
					try {
						String url = "/setoption/selectshow/" + hyperlink.getText();
						
						RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
						requestBuilder.sendRequest(null, new RequestCallback() {
							public void onResponseReceived(Request request, Response response) {
								if (200 != response.getStatusCode()) {
									Window.alert("Error: " + response.getStatusText());
								}
							}
							
							public void onError(Request request, Throwable e) {
								Window.alert("Error: " + e.getMessage());
							}
						});
					} catch (RequestException e) {
						Window.alert("Error: " + e.getMessage());
					}
				}
			});
			table.setWidget(row++, 0, hyperlink);
		}
		
		Button cancel = new Button("Cancel");
		cancel.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				hide();
			}
		});
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.add(table);
		verticalPanel.add(cancel);
		verticalPanel.setCellHorizontalAlignment(cancel, VerticalPanel.ALIGN_RIGHT);
		
		setWidget(verticalPanel);
	}
}
