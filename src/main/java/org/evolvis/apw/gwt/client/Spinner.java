package org.evolvis.apw.gwt.client;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.KeyboardListenerAdapter;
import com.google.gwt.user.client.ui.MouseListenerAdapter;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class Spinner extends HorizontalPanel {
	private Image upImage = new Image("/images/up.gif");
	private Image downImage = new Image("/images/down.gif");
	
	private VerticalPanel images = new VerticalPanel();
	private TextBox box = new TextBox();
	private int step = 20;
	private int max = 0;
	private int min = 0;
	private int curVal = 0;
	// delay to wait before increasing/decreasing further, in milliseconds
	private int delay = 120;
	
	private Timer t_raise = new Timer() {
		@Override
        public void run() {
			raise();
		}
	};
	private Timer t_lower = new Timer() {
		@Override
        public void run() {
			lower();
		}
	};
	
	public Spinner(int minVal, int maxVal, int stepVal, int startVal) {
		upImage.setPixelSize(10, 10);
		downImage.setPixelSize(10, 10);
		
		// Take over parameters
		step = stepVal;
		max = maxVal;
		min = minVal;
		
		// set curVal inside min or max bounds
		if (startVal < min) {
			curVal = min;
		} else if (startVal > max) {
			curVal = max;
		} else {
			curVal = startVal;
		}
		
		// Build our UI
		images.add(upImage);
		upImage.addMouseListener(new MouseListenerAdapter() {
			@Override
            public void onMouseDown(Widget sender, int x, int y) {
				t_raise.run();
				t_raise.scheduleRepeating(delay);
			}
			
			@Override
            public void onMouseUp(Widget sender, int x, int y) {
				t_raise.cancel();
			}
			
		});
		
		images.add(downImage);
		downImage.addMouseListener(new MouseListenerAdapter() {
			@Override
            public void onMouseDown(Widget sender, int x, int y) {
				t_lower.run();
				t_lower.scheduleRepeating(delay);
			}
			
			@Override
            public void onMouseUp(Widget sender, int x, int y) {
				t_lower.cancel();
			}
			
		});
		
		updateTextBox();
		box.addKeyboardListener(new KeyboardListenerAdapter() {
			@Override
            public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				TextBox tb = (TextBox) sender;
				int index = tb.getCursorPos();
				String str = tb.getText();
				
				// If no number has been entered, ignore it
				if (!Character.isDigit(keyCode)) {
					tb.cancelKey();
					return;
				}
				
				// If we have 0123 as input and 123 is selected, we need to
				// calculate with "09" if "9" was entered
				String newstr;
				if (tb.getSelectionLength() > 0) {
					// str = str.replaceFirst(str.substring(tb.getCursorPos(),
					// tb.getSelectionLength()+1), "");
					newstr = str.substring(0, tb.getCursorPos());
					newstr += keyCode;
					newstr += str.substring(tb.getCursorPos() + tb.getSelectionLength(), str.length());
					
				} else {
					
					newstr = str.substring(0, index) + keyCode + str.substring(index, str.length());
				}
				
				int newint = Integer.parseInt(newstr);
				
				// If we are not inside the bounds, leave this method
				// TODO: Choose, whether to display the maxvalue or leave
				if (newint > max || newint < min) {
					tb.cancelKey();
					return;
				}
				
				curVal = newint;
				
			}
		});
		
		// set width analog to max length of the maximum value
		box.setWidth((((max + "").length() + 1) / 2) + 1 + "em");
		box.setTextAlignment(TextBoxBase.ALIGN_RIGHT);
		add(box);
		add(images);
	}
	
	private void updateTextBox() {
		box.setText(curVal + "");
		if (curVal == max) {
			t_raise.cancel();
		} else if (curVal == min) {
			t_lower.cancel();
		}
	}
	
	private void raise() {
		if (curVal + step < max) {
			curVal += step;
		} else {
			curVal = max;
		}
		updateTextBox();
	}
	
	private void lower() {
		if (curVal - step > min) {
			curVal -= step;
		} else {
			curVal = min;
		}
		updateTextBox();
	}
	
	public void setDelay(int delayMilliSeconds) {
		delay = delayMilliSeconds;
	}
}
