package org.evolvis.apw.gwt.client;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ChooseCategoryDialog extends PopupPanel {
	private static final String[] VALUES = new String[] {
			"<strong>All</strong>",
			"<strong>Apparel</strong>",
			"<strong>Automotive</strong>",
			"<strong>Baby</strong>",
			"<strong>Beauty</strong>",
			"<strong>Blended</strong>",
			"<strong>Books</strong>",
			"<strong>Classical</strong>",
			"<strong>DigitalMusic</strong>",
			"<strong>DVD</strong>",
			"<strong>Electronics</strong>",
			"<strong>ForeignBooks</strong>",
			"<strong>GourmetFood</strong>",
			"<strong>Grocery</strong>",
			"<strong>HealthPersonalCare</strong>",
			"<strong>Hobbies</strong>",
			"<strong>HomeGarden</strong>",
			"<strong>Industrial</strong>",
			"<strong>Jewelry</strong>",
			"<strong>KindleStore</strong>",
			"<strong>Kitchen</strong>",
			"<strong>Magazines</strong>",
			"<strong>Merchants</strong>",
			"<strong>Miscellaneous</strong>",
			"<strong>MP3Downloads</strong>",
			"<strong>Music</strong>",
			"<strong>MusicalInstruments</strong>",
			"<strong>MusicTracks</strong>",
			"<strong>OfficeProducts</strong>",
			"<strong>OutdoorLiving</strong>",
			"<strong>PCHardware</strong>",
			"<strong>PetSupplies</strong>",
			"<strong>Photo</strong>",
			"<strong>SilverMerchant</strong>",
			"<strong>Software</strong>",
			"<strong>SoftwareVideoGames</strong>",
			"<strong>SportingGoods</strong>",
			"<strong>Tools</strong>",
			"<strong>Toys</strong>",
			"<strong>VHS</strong>",
			"<strong>Video</strong>",
			"<strong>VideoGames</strong>",
			"<strong>Watches</strong>",
			"<strong>Wireless</strong>",
			"<strong>WirelessAccessories</strong>" };
	private static final int ITEMS_PER_COLUMN =
			(VALUES.length / 3) +
			(((VALUES.length % 3) == 0) ? 1 : 2);

	public ChooseCategoryDialog(final HasText senderWidget) {
		super(true);
		
		FlexTable table = new FlexTable();
		table.setCellPadding(0);
		table.setCellSpacing(0);
		
		int row = 0;
		int column = 0;
		
		table.getFlexCellFormatter().setColSpan(0, 0, 3);
		
		for (String title : VALUES) {
			final Hyperlink hyperlink = new Hyperlink();
			hyperlink.setHTML(title);
			hyperlink.addClickListener(new ClickListener() {
				public void onClick(Widget sender) {
					if (senderWidget != null)
						senderWidget.setText(hyperlink.getText());
					hide();
					try {
						String url = "/setoption/selectcategory/" + hyperlink.getText();
						
						RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
						requestBuilder.sendRequest(null, new RequestCallback() {
							public void onResponseReceived(Request request, Response response) {
								if (200 != response.getStatusCode()) {
									Window.alert("Error: " + response.getStatusText());
								}
							}
							
							public void onError(Request request, Throwable e) {
								Window.alert("Error: " + e.getMessage());
							}
						});
					} catch (RequestException e) {
						Window.alert("Error: " + e.getMessage());
					}
				}
			});
			table.setWidget(row++, column, hyperlink);
			if (row == ITEMS_PER_COLUMN) {
				row = 1;
				column++;
			}
		}
		
		Button cancel = new Button("Cancel");
		cancel.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				hide();
			}
		});
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.add(table);
		verticalPanel.add(cancel);
		verticalPanel.setCellHorizontalAlignment(cancel, VerticalPanel.ALIGN_RIGHT);
		
		setWidget(verticalPanel);
	}
}
