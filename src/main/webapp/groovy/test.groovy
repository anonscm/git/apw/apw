<%
	import net.sourceforge.stripes.controller.*;

	println "<strong>request:</strong> " + request.getClass().toString() + "<br/>";
	println "<pre>" + request.toString() + "</pre><br/>";
	
	println "<strong>request.request</strong>: " + request.request.getClass().toString() + "<br/>";
	println "<pre>" + request.request.toString() + "</pre><br/>";
	
	println "<strong>response:</strong> " + response.getClass().toString() + "<br/>";
	println "<pre>" + response.toString() + "</pre><br/>";
	
	println "<strong>session:</strong> " + session.getClass().toString() + "<br/>";
	println "<pre>" + session.toString() + "</pre><br/>";
	
	println "<strong>params:</strong> " + params.getClass().toString() + "<br/>";
	println "<pre>" + params.toString() + "</pre><br/>";
	
	println "application: " + application.toString() + "<br/>";
	
	println "out: " + out.toString() + "<br/>";
	
	out.print("out.print()<br/>");
	
	println "Configuration: " + StripesFilter.getConfiguration().toString() + "<br/>";
	
	println "ActionBean: " + request.getAttribute(StripesConstants.REQ_ATTR_ACTION_BEAN).toString() + "<br/>";
	
	println "ActionBeanContext: " + request.getAttribute(StripesConstants.REQ_ATTR_ACTION_BEAN).getContext().toString() + "<br/>";

%>